# Pyorder

Tiny project that order the elements of a given folder path

## Project Structure

```bash 
pyorder/
├── src/
│ └── main.py
├── README.md
└── requirements.txt
```



## Requirements

- Python 3.6 or higher

## Installation

1. Clone the repository:

```bash
   git clone https://github.com/your-username/file_organizer.git
   cd file_organizer
```
Create and activate a virtual environment (optional but recommended):

```bash
python -m venv venv
source venv/bin/activate  # On Windows use `venv\Scripts\activate`
```

Install the required dependencies:

```bash
pip install -r requirements.txt
```

## Usage

To use the script, run the following command from the root of the project:

```bash
python src/main.py --path /path/to/your/directory
```

Replace /path/to/your/directory with the actual path to the directory containing the files you want to organize.

How It Works
Argument Parsing:

The script uses argparse to handle the command-line argument --path, which specifies the directory to process.
File Sorting:

The script reads all files in the specified directory and sorts them by their modification date.
File Renaming:

The script renames the files sequentially using a counter starting from zero, preserving their original file extensions.
Example
Given a directory /path/to/your/directory with files:

```bash
file_b.txt
file_a.txt
file_c.txt
```
After running the script, the files will be renamed (assuming file_a.txt is the oldest and file_c.txt is the newest):


```bash
0.txt
1.txt
2.txt
```

# Contributing
Contributions are welcome! Please feel free to submit a Pull Request.

### License
This project is licensed under the MIT License. See the LICENSE file for details.