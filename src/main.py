import os
import sys
import argparse
from pathlib import Path

def get_files_sorted_by_date(directory):
    """
    Get a list of files in a directory, sorted by modification date.
    """
    try:
        files = [f for f in directory.iterdir() if f.is_file()]
        files.sort(key=lambda x: x.stat().st_mtime)
        return files
    except Exception as e:
        print(f"Error al obtener archivos: {e}")
        sys.exit(1)

def rename_files(files):
    """
    Rename files with a counter from 0 to the maximum number of files.
    """
    try:
        for index, file in enumerate(files):
            new_name = file.parent / f"{index}{file.suffix}"
            file.rename(new_name)
            print(f"Renombrado: {file} -> {new_name}")
    except Exception as e:
        print(f"Error al renombrar archivos: {e}")
        sys.exit(1)

def main():
    # Parsed arguments
    parser = argparse.ArgumentParser(description="Order files by date and rename them with a counter.")
    parser.add_argument('--path', required=True, help="Path to the directory with the files.")
    args = parser.parse_args()

    # Verify that the path is a directory
    directory = Path(args.path)
    if not directory.is_dir():
        print(f"Error: The given route '{args.path}' is not a directory")
        sys.exit(1)

    # Get files sorted by date
    files = get_files_sorted_by_date(directory)

    # Rename files
    rename_files(files)

if __name__ == "__main__":
    main()
